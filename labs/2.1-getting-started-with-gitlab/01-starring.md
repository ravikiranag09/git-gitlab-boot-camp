# Starring Repositories

Find a repository you find interesting and click the Star button:

![Starring](../../img/gitlab-star01.png)

Find your starred repositories on your Dashboard:

![Your stars](../../img/gitlab-star02.png)

You should see an overview of all repositories you have given a star:

